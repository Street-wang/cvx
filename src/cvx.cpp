/**
 * @file cvx.cpp
 * @brief MPC trajectory generator using CVXGEN
 * @author Brett Lopez <btlopez@mit.edu>
 * @author Parker Lusk <plusk@mit.edu>
 * @date 19 January 2020
 */

#include <iostream>
#include "cvx/cvx.h"

extern "C" {
#include "solver.h"
}

///< \brief Global variables required by CVXGEN solver
Vars vars;
Params params;
Workspace work;
Settings settings;

namespace acl {
namespace cvx {

CVX::CVX(double control_dt, double u_max, double qf, double term_thr)
: control_dt_(control_dt), u_max_(u_max), qf_(qf), term_thr_(term_thr),
  A_(nullptr), B_(nullptr), Qf_(nullptr), x0_(nullptr),
  xf_(nullptr), X_(nullptr), U_(nullptr)
{
  //
  // Initialize CVXGEN solver
  //

  set_defaults();
  setup_indexing();

  // quiet solver
  settings.verbose = 0;

  //
  // map CVXGEN memory to Eigen (COLMAJOR to COLMAJOR)
  //

  new (&A_) Eigen::Map<Amat>(params.A, dim::n, dim::n);
  new (&B_) Eigen::Map<Bmat>(params.B, dim::n, dim::m);
  new (&Qf_) Eigen::Map<Qmat>(params.Q_final, dim::n, dim::n);
  new (&x0_) Eigen::Map<State>(params.x_0, dim::n);
  new (&xf_) Eigen::Map<State>(params.xf, dim::n);

  // There is no x[0] variable, so skip the first timestep (column)
  new (&X_) Eigen::Map<StateStack>(&vars.x[1][0], dim::n, dim::N);
  new (&U_) Eigen::Map<CommandStack>(&vars.u[0][0], dim::m, dim::N+1);

  //
  // Set fixed problem parameters
  //

  params.u_max[0] = u_max_;

  // set the terminal cost: a soft constraint on ending at the desired waypoint
  Qf_.diagonal().array() = qf_;
}

// ----------------------------------------------------------------------------

void CVX::generateTrajectory(const State& x0, const State& xf, TrajXd& traj)
{
  // set problem data
  x0_ = x0;
  xf_ = xf;

  double dt = optimize();

  interpolate(dt, traj);
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

double CVX::optimize()
{
  static constexpr double DT_INC = 0.025;

  // perform a line search on the timestep to use
  double dt = 0.0;
  while (dt == 0.0 || !hasConverged()) { // always run at least once

    // since we can't increase the number of timesteps in the horizon,
    // the only way to give the optimizer more time to reach xf is to
    // coarsen the discretization of the dynamics.
    dt += DT_INC;
    discretizeLTISystem(dt, A_, B_);

    // Use CVXGEN to solve the problem description with the current timestep
    solve();
  }

  return dt;
}

// ----------------------------------------------------------------------------

void CVX::discretizeLTISystem(double dt,
                              Eigen::Ref<Amat> A, Eigen::Ref<Bmat> B) const
{
  // double integrator dynamics (NCV)
  A.setIdentity();
  A.diagonal<3>().array() = dt;

  // input matrix (acceleration commands)
  B.setZero();
  B.diagonal().array() = 0.5*dt*dt;
  B.diagonal<-3>().array() = dt;
}

// ----------------------------------------------------------------------------

bool CVX::hasConverged() const
{
  // calculate terminal cost
  double tc = qf_ * (X_.rightCols<1>() - xf_).squaredNorm();

  // we have converged if terminal cost is below an acceptable threshold
  return tc < term_thr_;
}

// ----------------------------------------------------------------------------

void CVX::interpolate(double dt, TrajXd& traj) const
{
  // Determine how many control timesteps are there in the MPC horizon
  const size_t NN = static_cast<int>(dim::N * dt / control_dt_);

  // trajectory interpolated from MPC solution
  traj = TrajXd::Zero(dim::n+dim::m, NN);

  // discretize our LTI system model to generate the interpolated trajectory
  Amat Ad;
  Bmat Bd;
  discretizeLTISystem(control_dt_, Ad, Bd);

  // To generate the interpolated trajectory, we perform a zero-order hold on
  // the MPC command (i.e., the highest-order derivative) and then numerically
  // integrate at the control period to obtain a smooth trajectory.
  // The integration is performed by propagating our discretized system model.
  for (size_t i=0; i<NN; ++i) {
    // index of current MPC timestep
    const size_t j = std::floor(i * control_dt_ / dt);

    // zero-order hold of MPC command
    traj.col(i).tail<dim::m>() = U_.col(j);

    if (i == 0) {
      // trajectory starts at the initial condition
      traj.col(i).head<dim::n>() = x0_;
    } else {
      traj.col(i).head<dim::n>() = Ad*traj.col(i-1).head<dim::n>()
                                    + Bd*traj.col(i-1).tail<dim::m>();
    }
  }
}

} // ns cvx
} // ns acl
