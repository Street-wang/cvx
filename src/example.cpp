/**
 * @file example.h
 * @brief Example of how to use a CVXGEN library with C++/Eigen
 * @author Parker Lusk <plusk@mit.edu>
 * @date 18 January 2020
 */

#include <iostream>

#include <Eigen/Dense>

extern "C" {
#include "solver.h"
}

///< \brief Global variables required by CVXGEN solver
Vars vars;
Params params;
Workspace work;
Settings settings;

int main(int argc, char *argv[])
{
  // initialize the optimizer
  set_defaults();
  setup_indexing();

  // quiet solver
  settings.verbose = 0;

  // parameters from the CVXGEN problem description
  static constexpr int n = 6;
  static constexpr int m = 3;
  static constexpr int N = 20;

  // set fixed problem parameters
  params.u_max[0] = 5;
  double dt = 0.1;

  // map CVXGEN params memory to Eigen (COLMAJOR to COLMAJOR)
  Eigen::Map<Eigen::Matrix<double, n, n>> A(params.A, n, n);
  Eigen::Map<Eigen::Matrix<double, n, m>> B(params.B, n, m);
  Eigen::Map<Eigen::Matrix<double, n, n>> Qf(params.Q_final, n, n);
  Eigen::Map<Eigen::Matrix<double, n, 1>> xf(params.xf, n);
  // example of mapping later (useful for class member variables)
  Eigen::Map<Eigen::Matrix<double, n, 1>> x0(nullptr);
  new (&x0) Eigen::Map<Eigen::Matrix<double, n, 1>>(params.x_0, n);

  // Map variables (answers)
  Eigen::Map<Eigen::Matrix<double, m, N+1>> U(&vars.u[0][0], m, N+1);
  // There is no x_0, so skip the first column
  Eigen::Map<Eigen::Matrix<double, n, N>> X(&vars.x[1][0], n, N);

  //
  // Define the problem (this would happen each time a waypoint is sent)
  //

  // we really care about ending where we specify
  Qf.diagonal().array() = 100000;

  // double integrator dynamics (NCV)
  A.setIdentity();
  A.diagonal<3>().array() = dt;

  // input matrix (acceleration commands)
  B.diagonal().array() = 0.5*dt*dt;
  B.diagonal<-3>().array() = dt;

  // start at (0, 0, 0) with zero velocity
  x0.setZero();

  // end at be at (2, 2, 2) with zero velocity
  xf.setZero();
  xf.head<3>().array() = 2;

  //
  // Solve the problem
  //

  std::cout << U.transpose() << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << X.transpose() << std::endl;

  solve();

  std::cout << U.transpose() << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << X.transpose() << std::endl;

  return 0;
}
