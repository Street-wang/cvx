/**
 * @file cvx_ros.cpp
 * @brief ROS wrapper for cvx trajectory generator
 * @author Brett Lopez <btlopez@mit.edu>
 * @author Parker Lusk <plusk@mit.edu>
 * @date 19 January 2020
 */

#include "cvx/cvx_ros.h"

namespace acl {
namespace cvx {

CVX_ROS::CVX_ROS(const ros::NodeHandle& nh, const ros::NodeHandle& nhp)
: nh_(nh), nhp_(nhp)
{

  init();

  //
  // ROS pub / sub communication
  //

  pub_cmdout_ = nh_.advertise<snapstack_msgs::Goal>("goal",1);
  pub_path_ = nh_.advertise<nav_msgs::Path>("path",1);
  pub_vizsetpoint_ = nh_.advertise<visualization_msgs::Marker>("viz_setpoint",1);

  sub_waypoint_ = nh_.subscribe("waypoint", 1, &CVX_ROS::waypointCb, this);
  sub_fmode_ = nh_.subscribe("/globalflightmode", 1, &CVX_ROS::flightmodeCb, this);
  sub_state_ = nh_.subscribe("state", 1, &CVX_ROS::stateCb, this);

  //
  // Timers
  //

  tim_control_ = nh_.createTimer(ros::Duration(control_dt_),
                                                &CVX_ROS::controlCb, this);
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void CVX_ROS::init()
{
  //
  // Load Parameters
  //

  // room bounds
  nh_.param<double>("/room_bounds/x_min", bounds_x_min_, 0.0);
  nh_.param<double>("/room_bounds/x_max", bounds_x_max_, 1.0);
  nh_.param<double>("/room_bounds/y_min", bounds_y_min_, 0.0);
  nh_.param<double>("/room_bounds/y_max", bounds_y_max_, 1.0);
  nh_.param<double>("/room_bounds/z_min", bounds_z_min_, 0.0);
  nh_.param<double>("/room_bounds/z_max", bounds_z_max_, 1.0);

  // general takeoff and landing params
  nh_.param<double>("cntrl/spinup_time", spinup_time_, 2.0);
  nhp_.param<double>("control_dt", control_dt_, 0.01);
  nhp_.param<double>("takeoff_inc", takeoff_inc_, 0.0035);
  nhp_.param<double>("takeoff_alt", takeoff_alt_, 1.0);
  nhp_.param<bool>("takeoff_rel", takeoff_rel_, false);
  nhp_.param<double>("landing_fast_threshold", landing_fast_threshold_, 0.400);
  nhp_.param<double>("landing_fast_dec", landing_fast_dec_, 0.0035);
  nhp_.param<double>("landing_slow_dec", landing_slow_dec_, 0.001);

  // cvx demo specific
  nhp_.param<bool>("use_feedforward", use_feedforward_, false);

  double u_max, qf, term_thr;
  nhp_.param<double>("u_max", u_max, 1.0);
  nhp_.param<double>("q_final", qf, 100000);
  nhp_.param<double>("terminal_threshold", term_thr, 4000);

  //
  // Initialize the trajectory generator
  //

  cvx_.reset(new CVX(control_dt_, u_max, qf, term_thr));

  //
  // Visualization initialization
  //

  // Initialize setpoint marker
  vizsetpoint_.id = 0;
  vizsetpoint_.type = visualization_msgs::Marker::SPHERE;
  vizsetpoint_.pose.orientation.x = 0.0;
  vizsetpoint_.pose.orientation.y = 0.0;
  vizsetpoint_.pose.orientation.z = 0.0;
  vizsetpoint_.pose.orientation.w = 1.0;
  vizsetpoint_.scale.x = 0.35;
  vizsetpoint_.scale.y = 0.35;
  vizsetpoint_.scale.z = 0.35;
  vizsetpoint_.color.a = 0.7; // Don't forget to set the alpha!
  vizsetpoint_.color.r = 1.0;
  vizsetpoint_.color.g = 0.5;
  vizsetpoint_.color.b = 0.0;
}

// ----------------------------------------------------------------------------

void CVX_ROS::flightmodeCb(const snapstack_msgs::QuadFlightModeConstPtr& msg)
{
  // handle safety state machine transitions based on
  // global flight modes dispatched by the operator.

  if (mode_ == Mode::NOT_FLYING &&
        msg->mode == snapstack_msgs::QuadFlightMode::GO) {
    mode_ = Mode::TAKEOFF;
    ROS_INFO("Spinning up motors for takeoff...");

  } else if ((mode_ == Mode::TAKEOFF || mode_ == Mode::FLYING) &&
        msg->mode == snapstack_msgs::QuadFlightMode::LAND) {
    mode_ = Mode::LANDING;
    ROS_INFO("Landing...");

  } else if (msg->mode == snapstack_msgs::QuadFlightMode::KILL) {
    mode_ = Mode::NOT_FLYING;
    ROS_WARN("Killing!");

  }
}

// ----------------------------------------------------------------------------

void CVX_ROS::waypointCb(const ::cvx::WaypointConstPtr& msg)
{
  State x0;
  x0(0) = pose_.pose.position.x;
  x0(1) = pose_.pose.position.y;
  x0(2) = pose_.pose.position.z;
  // TODO: If in the middle of a trajectory, use the goal vel/accel instead
  x0(3) = twist_.twist.linear.x;
  x0(4) = twist_.twist.linear.y;
  x0(5) = twist_.twist.linear.z;

  // make sure that the desired waypoint is not out of bounds
  snapstack_msgs::Goal tmp;
  tmp.p = msg->pos;
  tmp.v = msg->vel;
  makeGoalSafe(tmp);

  State xf;
  xf(0) = tmp.p.x;
  xf(1) = tmp.p.y;
  xf(2) = tmp.p.z;
  xf(3) = tmp.v.x;
  xf(4) = tmp.v.y;
  xf(5) = tmp.v.z;

  cvx_->generateTrajectory(x0, xf, traj_);
  replanned_ = true;
  pubTraj();
}

// ----------------------------------------------------------------------------

void CVX_ROS::stateCb(const snapstack_msgs::StateConstPtr& msg)
{
  pose_.header = msg->header;
  pose_.pose.position.x = msg->pos.x;
  pose_.pose.position.y = msg->pos.y;
  pose_.pose.position.z = msg->pos.z;
  pose_.pose.orientation.x = msg->quat.x;
  pose_.pose.orientation.y = msg->quat.y;
  pose_.pose.orientation.z = msg->quat.z;
  pose_.pose.orientation.w = msg->quat.w;

  twist_.header = msg->header;
  twist_.twist.linear = msg->vel;
  twist_.twist.angular = msg->w;
}

// ----------------------------------------------------------------------------

void CVX_ROS::controlCb(const ros::TimerEvent& e)
{
  static snapstack_msgs::Goal goalmsg;
  static bool flight_initialized = false;
  static ros::Time takeoff_time;
  static double last_active_goal_time;
  static double takeoff_alt;
  static double initial_alt;

  if (mode_ == Mode::TAKEOFF) {

    if (!flight_initialized) {
      // capture the initial time
      takeoff_time = ros::Time::now();

      // set the goal to our current position + yaw
      goalmsg.p.x = pose_.pose.position.x;
      goalmsg.p.y = pose_.pose.position.y;
      goalmsg.p.z = pose_.pose.position.z;
      goalmsg.v.x = 0;
      goalmsg.v.y = 0;
      goalmsg.v.z = 0;
      goalmsg.yaw = tf2::getYaw(pose_.pose.orientation);
      goalmsg.dyaw = 0;

      // There is no real velocity control, since the ACL outer loop tracks
      // trajectories and their derivatives. To achieve velocity control,
      // we will integrate the velocity commands to obtain the trajectory.
      goalmsg.mode_xy = snapstack_msgs::Goal::MODE_POSITION_CONTROL;
      goalmsg.mode_z = snapstack_msgs::Goal::MODE_POSITION_CONTROL;

      // allow the outer loop to send low-level autopilot commands
      goalmsg.power = true;

      // what is our initial altitude before takeoff?
      initial_alt = pose_.pose.position.z;

      // what should our desired takeoff altitude be?
      takeoff_alt = takeoff_alt_ + ((takeoff_rel_) ? initial_alt : 0.0);

      flight_initialized = true;
    }

    // wait for the motors to spin up all the way before sending a command
    if (ros::Time::now() - takeoff_time >= ros::Duration(spinup_time_)) {

      constexpr double TAKEOFF_THRESHOLD = 0.100;
      if ((std::abs(goalmsg.p.z - pose_.pose.position.z) < TAKEOFF_THRESHOLD) &&
            std::abs(goalmsg.p.z - takeoff_alt) < TAKEOFF_THRESHOLD) {
        mode_ = Mode::FLYING;
        ROS_INFO("Takeoff complete!");

        // reset the trajectory
        traj_.resize(Eigen::NoChange, 0);
        pubTraj();
      } else {
        // Increment the z cmd each timestep for a smooth takeoff.
        // This is essentially saturating tracking error so actuation is low.
        goalmsg.p.z = clamp(goalmsg.p.z + takeoff_inc_, 0.0, takeoff_alt);
      }
    }

  } else if (mode_ == Mode::FLYING) {

    // start from beginning of trajectory if we got a new plan
    if (replanned_) { k_ = 0; replanned_ = false; }

    // if an optimized trajectory exists
    if (traj_.cols() > 0) {
      goalmsg.p.x = traj_(0,k_);
      goalmsg.p.y = traj_(1,k_);
      goalmsg.p.z = traj_(2,k_);
      goalmsg.v.x = traj_(3,k_);
      goalmsg.v.y = traj_(4,k_);
      goalmsg.v.z = traj_(5,k_);
      if (use_feedforward_) {
        goalmsg.a.x = traj_(6,k_);
        goalmsg.a.y = traj_(7,k_);
        goalmsg.a.z = traj_(8,k_);

        // if jerk was produced
        if (traj_.rows() > 9) {
          goalmsg.j.x = traj_(9,k_);
          goalmsg.j.y = traj_(10,k_);
          goalmsg.j.z = traj_(11,k_);
        }
      }

      // proceed to next timestep until the end of the trajectory
      if (k_ < traj_.cols()-1) ++k_;
    }

    // make sure we don't hit any walls
    makeGoalSafe(goalmsg);

    // Visualize current setpoint goal of trajectory
    vizsetpoint_.header.stamp = ros::Time::now();
    vizsetpoint_.header.frame_id = pose_.header.frame_id;
    vizsetpoint_.pose.position.x = goalmsg.p.x;
    vizsetpoint_.pose.position.y = goalmsg.p.y;
    vizsetpoint_.pose.position.z = goalmsg.p.z;
    pub_vizsetpoint_.publish(vizsetpoint_);

  } else if (mode_ == Mode::LANDING) {

    goalmsg.v.x = goalmsg.v.y = goalmsg.v.z = 0;
    goalmsg.dyaw = 0;

    // choose between fast landing and slow landing
    const double thr = landing_fast_threshold_ + ((takeoff_rel_) ?
                                    initial_alt : 0.0);
    const double dec = (pose_.pose.position.z > thr) ?
                                    landing_fast_dec_ : landing_slow_dec_;

    goalmsg.p.z = clamp(goalmsg.p.z - dec, -0.1, bounds_z_max_);

    // TODO: Make this closed loop (maybe vel based?)
    if (goalmsg.p.z == -0.1) {
      mode_ = Mode::NOT_FLYING;
      ROS_INFO("Landing complete!");
    }

  } else if (mode_ == Mode::NOT_FLYING) {
    goalmsg.power = false;
    flight_initialized = false;
  }

  goalmsg.header.stamp = ros::Time::now();
  goalmsg.header.frame_id = "body";
  pub_cmdout_.publish(goalmsg);
}

// ----------------------------------------------------------------------------

void CVX_ROS::pubTraj() const
{
  nav_msgs::Path path;
  path.header.stamp = ros::Time::now();
  path.header.frame_id = pose_.header.frame_id;

  for (size_t i=0; i<traj_.cols(); ++i) {
    geometry_msgs::PoseStamped pose;
    pose.header.stamp = ros::Time(i*control_dt_);
    pose.header.frame_id = path.header.frame_id;
    pose.pose.position.x = traj_(0,i);
    pose.pose.position.y = traj_(1,i);
    pose.pose.position.z = traj_(2,i);
    pose.pose.orientation.w = 1;
    pose.pose.orientation.x = 0;
    pose.pose.orientation.y = 0;
    pose.pose.orientation.z = 0;
    path.poses.push_back(pose);
  }

  pub_path_.publish(path);
}

// ----------------------------------------------------------------------------

void CVX_ROS::makeGoalSafe(snapstack_msgs::Goal& goalmsg) const
{
  // n.b.: this isn't particularly intelligent. It just instantaneously stops
  // the vehicle (i.e., infinite acceleration change). However, for
  // trajectories that barely stray outside of the bounds, this should be
  // sufficient. Besides, out of bounds waypoints should not be allowed.

  bool unsafe_x = goalmsg.p.x<bounds_x_min_ || goalmsg.p.x>bounds_x_max_;
  bool unsafe_y = goalmsg.p.y<bounds_y_min_ || goalmsg.p.y>bounds_y_max_;
  bool unsafe_z = goalmsg.p.z<bounds_z_min_ || goalmsg.p.z>bounds_z_max_;

  // clamp position of trajectory to inside edge of room
  goalmsg.p.x = clamp(goalmsg.p.x, bounds_x_min_, bounds_x_max_);
  goalmsg.p.y = clamp(goalmsg.p.y, bounds_y_min_, bounds_y_max_);
  goalmsg.p.z = clamp(goalmsg.p.z, bounds_z_min_, bounds_z_max_);

  // get rid of higher-order motion in unsafe directions
  if (unsafe_x) goalmsg.v.x = goalmsg.a.x = goalmsg.j.x = 0;
  if (unsafe_y) goalmsg.v.y = goalmsg.a.y = goalmsg.j.y = 0;
  if (unsafe_z) goalmsg.v.z = goalmsg.a.z = goalmsg.j.z = 0;
}

// ----------------------------------------------------------------------------

double CVX_ROS::clamp(double d, double min, double max) const
{
  const double t = d < min ? min : d;
  return t > max ? max : t;
}

} // ns cvx
} // ns acl
