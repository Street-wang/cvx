#pragma once

#include <cmath>

#include <Eigen/Dense>

namespace acl {
namespace cvx {

  /// \brief Dimensions used in CVXGEN problem description
  namespace dim {
    static constexpr int N = 20; ///< number of timesteps in horizon
    static constexpr int n = 6; ///< dim of state (x, y, z, vx, vy, vz)
    static constexpr int m = 3; ///< dim of input (ax, ay, az)
  } // ns dim

  /// \brief Eigen matrix types with problem dimensions
  using Amat = Eigen::Matrix<double, dim::n, dim::n>;
  using Bmat = Eigen::Matrix<double, dim::n, dim::m>;
  using Qmat = Eigen::Matrix<double, dim::n, dim::n>;
  using State = Eigen::Matrix<double, dim::n, 1>;
  using Command = Eigen::Matrix<double, dim::m, 1>;
  using StateStack = Eigen::Matrix<double, dim::n, dim::N>;
  using CommandStack = Eigen::Matrix<double, dim::m, dim::N+1>;
  using TrajXd = Eigen::Matrix<double, dim::n+dim::m, Eigen::Dynamic>;

  class CVX
  {
  public:
    CVX(double control_dt, double u_max, double qf, double term_thr);
    ~CVX() = default;

    /**
     * @brief      Given initial and final conditions, generate a trajectory
     *
     * @param[in]  x0    Initial state
     * @param[in]  xf    Final state
     * @param      traj  The optimized and interpolated trajectory
     */
    void generateTrajectory(const State& x0, const State& xf, TrajXd& traj);
  private:

    /// \brief Parameters
    double control_dt_; ///< control period for interpolation
    double u_max_; ///< input upper limit constraint
    double qf_; ///< terminal cost used as diagonal
    double term_thr_; ///< terminal cost threshold for convergence checking

    /// \brief Mapped memory
    Eigen::Map<Amat> A_; ///< state transition matrix (dynamics)
    Eigen::Map<Bmat> B_; ///< input matrix (how commands affect states)
    Eigen::Map<Qmat> Qf_; ///< p.s.d terminal cost for soft constraint
    Eigen::Map<State> x0_; ///< initial state
    Eigen::Map<State> xf_; ///< final state
    Eigen::Map<StateStack> X_; ///< states from x0 to xf, designed via MPC
    Eigen::Map<CommandStack> U_; ///< commands from x0 to xf, designed via MPC

    /**
     * @brief      Solve fixed-horizon MPC problem by performing a
     *             line search on the timestep to minimize the terminal cost
     *
     * @return     The timestep that led to achieving the final condition
     */ 
    double optimize();

    /**
     * @brief      Generate a discrete LTI system corresponding to the dynamic
     *             model used in the CVXGEN problem description.
     *
     * @param[in]  dt    Timestep used in discretization
     * @param[in]  A     Discretized system matrix
     * @param[in]  B     Discretized input matrix
     */
    void discretizeLTISystem(double dt,
                              Eigen::Ref<Amat> A, Eigen::Ref<Bmat> B) const;

    /**
     * @brief      Determines if converged.
     *
     * @return     True if terminal cost is below a user-defined threshold
     */
    bool hasConverged() const;

    /**
     * @brief      Densify a coarse trajectory at a finer period by integrating
     *             a zero-order hold (ZOH) sampling of the input.
     *
     * @param[in]  dt    The period of the coarse trajectory
     * @param      traj  The interpolated trajectory
     */
    void interpolate(double dt, TrajXd& traj) const;
  };

} // ns cvx
} // ns acl
