/**
 * @file cvx_ros.h
 * @brief ROS wrapper for cvx trajectory generator
 * @author Parker Lusk <plusk@mit.edu>
 * @date 19 January 2020
 */

#pragma once

#include <memory>

#include <ros/ros.h>

#include <tf2/utils.h>

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <nav_msgs/Path.h>
#include <visualization_msgs/Marker.h>
#include <snapstack_msgs/State.h>
#include <snapstack_msgs/Goal.h>
#include <snapstack_msgs/QuadFlightMode.h>
#include <cvx/Waypoint.h>

#include "cvx/cvx.h"

namespace acl {
namespace cvx {

  class CVX_ROS
  {
  public:
    CVX_ROS(const ros::NodeHandle& nh, const ros::NodeHandle& nhp);
    ~CVX_ROS() = default;

  private:
    ros::NodeHandle nh_, nhp_;
    ros::Publisher pub_cmdout_, pub_path_, pub_vizsetpoint_;
    ros::Subscriber sub_fmode_, sub_waypoint_, sub_state_;
    ros::Timer tim_control_;

    // cvx trajectory generator
    std::unique_ptr<CVX> cvx_;

    /// \brief Parameters
    double bounds_x_min_, bounds_x_max_; ///< safety bounds to
    double bounds_y_min_, bounds_y_max_; ///< keep the vehicle
    double bounds_z_min_, bounds_z_max_; ///< in the room.
    double control_dt_; ///< period at which outer loop commands are sent
    double spinup_time_; ///< how long to wait for motors to spin up
    double takeoff_inc_; ///< altitude increment used for smooth takeoff
    double takeoff_alt_; ///< desired takeoff altitude (maybe relative)
    bool takeoff_rel_; ///< should desired alt be relative to current alt?
    double landing_fast_threshold_; ///< above this alt, land "fast"
    double landing_fast_dec_; ///< use bigger decrements for "fast" landing
    double landing_slow_dec_; ///< use smaller decrements for "slow" landing
    bool use_feedforward_; ///< Include feed-forward command with goal

    /// \brief Internal state
    enum class Mode { NOT_FLYING, TAKEOFF, FLYING, LANDING };
    Mode mode_ = Mode::NOT_FLYING; ///< current mode derived from global flight mode
    geometry_msgs::PoseStamped pose_; ///< current pose of the vehicle
    geometry_msgs::TwistStamped twist_; ///< current twist of vehicle
    TrajXd traj_; ///< current valid trajectory from optimizer
    visualization_msgs::Marker vizsetpoint_; ///< viz of current setpoint
    bool replanned_; ///< whether or not a new plan has been made
    size_t k_; ///< current index of trajectory to execute

    /**
     * @brief      Load parameters and initialize cvx object
     */
    void init();

    /// \brief ROS callbacks
    void flightmodeCb(const snapstack_msgs::QuadFlightModeConstPtr& msg);
    void waypointCb(const ::cvx::WaypointConstPtr& msg);
    void stateCb(const snapstack_msgs::StateConstPtr& msg);
    void controlCb(const ros::TimerEvent& e);
    
    /**
     * @brief      Publish the optimized trajectory as a path for visualization
     */
    void pubTraj() const;

    /**
     * @brief      Naively enforce a goal to remain within the room bounds
     *
     * @param      goalmsg  The Goal msg to make safe
     */
    void makeGoalSafe(snapstack_msgs::Goal& goalmsg) const;

    /**
     * @brief      Ensure that a value remains inside bounds
     *
     * @param[in]  d     The value to clamp
     * @param[in]  min   The minimum allowable value
     * @param[in]  max   The maximum allowable value
     *
     * @return     d if within bounds, min or max if exceeds
     */
    double clamp(double d, double min, double max) const;
  };

}
}