CVXGEN Library
==============

After defining a problem using [CVXGEN](https://cvxgen.com), you can use the web application to generate C code. This directory contains (unchanged) generated C code from CVXGEN, along with a `CMakeLists.txt` used for building the library.

## Using CVXGEN with C++

Since CVXGEN generates C code, we create a shared library containing all of the C code, built with a C compiler. Then, we will link our C++ code against this shared library. A single header file (`solver.h`) is used to define the CVXGEN API. This header file must be included in the C++ project so that your code knows how to call the solver code. However, because your code is C++ and CVXGEN is C, you must include `solver.h` in a special way (because of C vs C++ linkage):

```c++
extern "C" {
#include "solver.h"
}
```

Another requirement when using CVXGEN is the creation of global variables:
```c++
///< \brief Global variables required by CVXGEN solver
Vars vars;
Params params;
Workspace work;
Settings settings;
```

These global variables create the memory that the QP solver in CVXGEN will use (avoiding dynamic allocation, which is slow). Of particular interest is `params` and `vars`.

The `params` struct is used to define the problem parameters (e.g., the dynamics matrix `A`, initial conditions, actuator limits, etc). The `vars` struct is used to retrieve solutions, i.e., the design variables.

Because these structs are used in C, they have very little *structure*. This just means that C arrays are being used for your data. Often, we use the `Eigen` matrix manipulation library in C++. Luckily, we can *map* the params into `Eigen` matrices and vectors. Said another way, `params` and `vars` is where the memory is actually created, but we can create a matrix *view* into this memory in a way that lets us do things like `A.setIdentity()` using the following syntax:
```c++
Eigen::Map<Eigen::Matrix<double, n, n>> A(params.A, n, n);
Eigen::Map<Eigen::Matrix<double, n, N>> X(&vars.x[1][0], n, N);
```

Then, we can happily set the problem data using `A` and retrieve the optimzed variables using `X`. Be sure to pay attention to the sizes of your memory---for example, there is no `x` at time 0, so we skip the first column by using `&vars.x[1][0]` (here each column represents a timestep)

See `example.cpp` for more.

## Adding Another CVXGEN Library

Download the zipped archive and extract into this directory. Rename the default `cvxgen` directory into something more specific (e.g., `accel` for first-order integrator dynamics with acceleration input). There is no need to modify the generated code.

To actually build this new library, be sure to add the appropriate lines to the `CMakeLists.txt`. Something like:
```cmake
add_library(cvxgen_accel accel/solver.c accel/ldl.c accel/matrix_support.c accel/util.c)
target_include_directories(cvxgen_accel PUBLIC accel)
```
